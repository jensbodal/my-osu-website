<!doctype html>
<html>
<?php 
    $HOME = "/nfs/stak/students/b/bodalj";
    $title = 'Unicode Characters';
    include($HOME.'/public_html/include/header.php');
?>
<div id="main">
&#x2227; = 2227 (AND) 
<br>
&#x2228; = 2228 (OR) 
<br>
&#x2295; = 2295 (XOR) 
<br>
&#x00ac; = 00ac (NOT)
<br>
&#x2192; = 2192 (IF) 
<br>
&#x2194; = 2194 (IFF) 
<br>
&#x2261; = 2261
<br>
&#x2283; = 2283
<br>
&#x21d2; = 21d2
<br>
&#x21d4; = 21d4
<br>
&#x2200; = 2200
<br>
&#x2203; = 2203
<br>
&#x2264; = 2264 (LTE) 
<br>
&#x2265; = 2265 (GTE) 
<br>
&#x2260; = 2260 (NE) 
<br>
&#x2229; = 2229 (INTERSECTION) 
<br>
&#x222a; = 222a (UNION) 
<br>
&#x2208; = 2208 (ELEMENT OF) 
<br>
&#x2209; = 2209 (NOT AN ELEMENT OF) 
<br>
&#x2282; = 2282 (SUBSET OF) 
<br>
&#x2283; = 2283 (SUPERSET OF) 
<br>
&#x2286; = 2286 (SUBSET OF OR EQUAL TO) 
<br>
&#x2287; = 2287 (SUPERSET OF OR EQUAL TO) 
<br>
&#x2205; = 2205 (EMPTY SET);
<br>
&#x2298; = 2298
<br>
&#x02e3; = 02e3
<br>
&#x03bb; = 03bb (Lambda | Empty Set)
<br>
&#x03a3; = 03a3 (Uppercase Sigma | Bit String set?)
<br>
&#x220E; = 220E (Proof Complete)
<br>
<br>
Not unicode but \bar in MS Word places the bar above what it comes after for set notation
<br>
<br>
<a href=http://kestrel.nmt.edu/~raymond/software/howtos/greekscape.xhtml">Greek Unicode</a>
<br>
<br>
<a href=word6x10Macro.txt>VBA Macro for quickly creating a table to use with Truth Tables & Proofs</a>
<br>
<br>
<a href="https://sites.google.com/site/projectcodebank/Do-It-Yourself/write-with-unicode-in-ms-word">Write unicode in MS Word</a>
<br>
<br>
<a href="http://www.fileformat.info/tip/microsoft/enter_unicode.htm">Enter Unicode</a>
<br>
<br>
<a href="http://symbolcodes.tlt.psu.edu/bylanguage/mathchart.html">Symbol Codes</a>
</div>

