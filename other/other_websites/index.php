<?php 
    $HOME = "/nfs/stak/students/b/bodalj";
    $title = 'Other OSU Student Sites';
    include($HOME.'/public_html/include/header.php');
?>

<div id="main">
<?php
    $myFile = fopen("otherWebsites.txt", "r") or die("Unable to open file");
    if ($myFile) {
        while (($line = fgets($myFile)) != false) {
            echo "<a href=\"$line\" target=_blank>$line</a>";
            echo "<br>";
        }
    }
    fclose($myFile);
?>
</div>
