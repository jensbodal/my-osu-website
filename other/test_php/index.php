<!doctype html>
<html>
<?php 
    $HOME = "/nfs/stak/students/b/bodalj";
    $title = 'Testing PHP Stuff';
    include($HOME.'/public_html/include/header.php');
?>

<div id="main">
<emph>Pass me a source variable as a URL</emph>
<br>
This page takes the input as a URL and returns the source code for that page.
<pre>
    http://web.engr.oregonstate.edu/~bodalj/other/test_php/index.php?<emph>source=<?php echo $_SERVER['SCRIPT_URI'] ?></emph>
</pre>
<?php 
    $source = $_GET['source'];
    $ch = curl_init($source);
    $fp = fopen("exampleInfo.txt", "w");

    curl_setopt($ch, CURLOPT_FILE, $fp);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_FAILONERROR, true);


    if (curl_exec($ch) === false) {
        echo 'Curl error: ' . curl_error($ch);
    }
    if (curl_errno($ch)) {
        echo 'erro'. $ch;
    }
    curl_close($ch);

    while(is_resource($fp)) {
        fclose($fp);
    }
    
    echo "<br>";
    $myFile = fopen("exampleInfo.txt", "r");
        if ($myFile) {
            echo '<pre><code class="language-markup">';
            while (($line = fgets($myFile)) != false) {
                $line = str_replace("<", "&lt", $line);
                echo $line;
            }
            echo '</code></pre>';
        }
    fclose($myFile);

?>
</div>

