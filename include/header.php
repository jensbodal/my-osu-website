<?php
    if (empty($title)) {
        $title = 'Random Testing Stuff';
    }
?>

<head>
    <title><?php echo $title ?></title>
    <link rel="stylesheet" href="/~bodalj/css/main.css">
    <link rel="stylesheet" href="/~bodalj/css/prism.css">
    <script src="/~bodalj/js/prism.js"></script>
    <script src="/~bodalj/js/prism.vim.js"></script>
    <link rel="icon" type"image/x-icon" href="/~bodalj/favicon.ico?v=4" />
</head>
<?php include 'menu.html' ?>
<?php include 'links.html' ?>
