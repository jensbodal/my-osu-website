<html>
<?php $title = 'Installing VIM from source' ?>
<?php include '../../include/header.php' ?>
<body>
<div id="main">
<h1>How to install VIM 7.4 from source</h1>
<explain>
In order to install Vim 7.4 you will need a terminal library such as ncurses,
which you will also need to compile from source.  Essentially what we are doing
is creating a new <emph>.local</emph> directory in our home directory to store our new 
installations, then tell PATH to look there before looking in the other 
directories (so that the older version of VIM that we can't uninstall is not
called).
</explain>
<pre>
<code class="language-bash">
wget ftp://ftp.vim.org/pub/vim/unix/vim-7.4.tar.bz2
wget http://ftp.gnu.org/pub/gnu/ncurses/ncurses-5.9.tar.gz
mkdir ~/.local
tar xvf ncurses*.gz
tar xvf vim*.bz2
cd ncurses-5.9
./configure --prefix=$HOME/.local
make && make install
cd ~/vim*
export CPPFLAGS="-I$HOME/.local/include/ncurses" 
export LDFLAGS="-L$HOME/.local/lib"
#Note that in some environments the export command will not work, instead issue the following (copy entire line and paste as one line):
bash -c "export CPPFLAGS='-I$HOME/.local/include/ncurses'; export LDFLAGS='-L$HOME/.local/lib'; ./configure --prefix=$HOME/.local --with-tlib=ncurses --enable-cscope; make && make install"
#End alternative installation. 
#Note Do not enable python interpreter if you do not need it 
./configure --prefix=$HOME/.local --with-tlib=ncurses --enable-cscope --enable-pythoninterp 
make && make install
VIM is now installed to ~/.local/bin
</code>
</pre>
<explain>
The last step is to add this new location to your PATH so that it is the first
area that bash will look for when running programs. Add the following to your
<emph>~/.bashrc</emph> and <emph>~/.bash_profile</emph> (or tell .bash_profile <a target=_blank href="http://stackoverflow.com/questions/415403/whats-the-difference-between-bashrc-bash-profile-and-environment">
to also read .bashrc</a>)
<pre>
    export PATH=~/.local/bin:$PATH 
</pre>
</explain>
<explain>
If you are using <emph>tcsh</emph> (FLIP) then you need to add the following to your <emph>~/.cshrc</emph> file: 
<pre>
    set path = (~/.local/bin $path)
</pre>
</explain>
</div>
</body>
</html>
