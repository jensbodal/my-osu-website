<!doctype html>
<html>
<?php $title = 'My VIM Setup' ?>
<?php include '../../include/header.php' ?>

<div id="main">
    <?php 
        $fp = fopen(".vimTableModeInstall.sh","r");
        if ($fp) {
        echo '<pre><code class="language-bash">';
        while (($line = fgets($fp)) != false) {
            echo $line;
            echo "<br>";
        }
        }
        echo '</code></pre>';
        fclose($fp);
    ?>
</div>
</html>
