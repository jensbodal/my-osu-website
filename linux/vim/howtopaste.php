<!doctype html>
<html>
<?php $title = 'My VIM Setup' ?>
<?php include '../../include/header.php' ?>

<div id="main">
p = paste after cursor
<br>
P = paste before cursor
<br>
<br>
You might notice that if you try to paste from an external file into a console
session of vim that the line spacing becomes "messed up".  To properly paste the
text while in insert mode, first issue the <emph>:set paste</emph> option then
try pasting the contents of the file. You can reverse this by either issuing
<emph>:set nopaste</emph> or exiting the file.  <a
href="http://vim.wikia.com/wiki/Toggle_auto-indenting_for_code_paste">Source</a>

</div>
</html>
