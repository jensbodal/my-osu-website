<!doctype html>
<html>
<?php $title = 'My VIM Setup' ?>
<?php include '../../include/header.php' ?>

<div id="main">
    Below is my current <emph><a href=".vimrc">.vimrc</a></emph> file (this is updated live as I make changes):
    <br>
    
    <?php 
        $fp = fopen(".vimrc","r");
        if ($fp) {
        echo '<pre><code class="language-vim">';
        while (($line = fgets($fp)) != false) {
            echo $line;
            echo "<br>";
        }
        }
        echo '</code></pre>';
        fclose($fp);
    ?>
    The <emph>.header</emph> file mentioned in the InsertCPPHeader() function  <emph><a href=".header">can be seen here</a></emph>
</div>
</html>
