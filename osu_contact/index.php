<?php $title = "OSU Contact Info" ?>
<?php include '../include/header.php' ?>
<div id="main">
<table class="t1" summary="OSU Contact Information">
    <caption>OSU Contact Information</caption>
    <thead>
        <tr><th>Student Service</th><th>Phone</th><th>Email</th></tr>
    </thead>
    <tfoot>
        <tr><th colspan="3">Formatting from <a href="http://www.the-art-of-web.com/html/table-markup/" target=_blank>the-art-of-web.com</a></th></tr>
    </tfoot>
    <tbody>
<?php
    $myFile = fopen("contactInfo.txt", "r") or die("Unable to open file");
    if ($myFile) {
        while (($line = fgets($myFile)) != false) {
            list($service, $phone, $email) = split (";", $line);

            echo "<tr><td>$service</td><td>$phone</td><td><a href=mailto:$email>$email</a></td></tr>";
        }
    }
    fclose($myFile);
?>
    </tbody>
</table>
</div>
