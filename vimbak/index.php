<!doctype html>
<html>
<?php $title = 'My VIM Setup' ?>
<?php include '../include/header.php' ?>

<div id="main">
    To see how to install Vim 7.4 from source <a href="vim.php">click here</a>
    <br>
    <br>
    My <emph>.vimrc</emph> file can be seen <a href="vimrc.php">here</a>
    <br>
    <br>
    <a href="http://robots.thoughtbot.com/vim-splits-move-faster-and-more-naturally" target=_blank>Good information on using screens in VIM</a>
</div>
</html>
