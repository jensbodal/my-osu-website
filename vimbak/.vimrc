" *****************************************************************************
" These options are being used in Vim 7.4. If using an earlier version of VIM
" please note that some of them (such as colorcolumn and undofile) won't work
" *****************************************************************************

"Turns on highlighting of syntax
:syntax on

"Sets shift ('<' and '>') column width
set shiftwidth=4                    

"Converts tabs to spaces
set expandtab                       

"Sets number of columns a tab equals
set softtabstop=4                   

"Keep same spacing on newline
set autoindent                      

"Color that is easier to see with dark background
:color desert                       

"Adds a column marker at 80 characters wide
set colorcolumn=80                 

"Adds line number marker
set number                          

"Sets 'gutter' column indent to 3 spaces
set numberwidth=3                  

"Background color which looks nice in putty
:colorscheme desert                 

"Allow backspace in insert mode
set backspace=indent,eol,start

"Shows incomplete cmds at bottom right while in visual mode
set showcmd

"Sets permanent undo directory, directory needs to exist beforehand
set undodir=~/.vimundo

"Turns on permanent undo history
set undofile

"Enables auto completion
set wildmenu

"First <Tab> will complete longest common string and invoke wildmenu, next
"tab will complete the first alternative and then will start to cycle through
set wildmode=longest:full,full

"Show line and column number as status text in bottom right
set ruler
    "%s/#INSERT_FILE_NAME#/\=expand('%r')/

"New keybindings for moving around with different screens
nnoremap <C-J> <C-W><C-J>
nnoremap <C-K> <C-W><C-K>
nnoremap <C-L> <C-W><C-L>
nnoremap <C-H> <C-W><C-H>

"Open new split panes to right and bottom, which feels more natural than Vim’s default:
set splitbelow
set splitright

" FUNCTIONS AND COMMANDS BELOW "

"This function will insert a custom header text for a cpp file
"The contents of the file in ~/class/cs161/.header will be inserted at the top of the file you're in, and will 
"automatically insert the filename and the date in the format: January 01, 1900
"A list of format options are found here: http://linux.die.net/man/3/strftime
" *****NOTE variable names are hardcored in the .header file for seach and replace*****
function! InsertCPPHeader()
    0r~/class/cs161/.header
    %s/#INSERT_FILE_NAME#/\=expand('%r')/
    %s/#INSERT_DATE#/\=strftime('%B %d, %Y')/
    normal G
endfunction

"This command will call the InsertCPPHeader() function. Would probably be best to call it something like CPPH 
"to denote it inserts the header for C++, but this suits my current needs.
"The command is called with :Header
command! Header :call InsertCPPHeader()

"When monitor is in portrait mode need a vertical split of 67 to maintain 80 character width
command! Vvsp :67vsp
